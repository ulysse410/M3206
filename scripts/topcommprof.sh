#!/bin/bash

cat my_history | awk '{print $2}' | cut -d ';' -f2 |sort | uniq -c | sort -nr | head -$1
#cat pour lire
#awk '{print $2}' pour afficher que la deuxieme colonne du fichier
# cut -d pour couper le reste
#sort pour avoir dans l'ordre alphabetique
#uniq -c pour compter le nombre de fois ou la commande est effectuer
# sort -nr pour l'odre croissant
#head -$1 pour avoir seulement les n passer en parametre


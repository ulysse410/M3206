#!/bin/bash

first=$(cat my_history | awk '{print $2}' | grep $1 | cut -d ';' -f2 | head -1)
#cat pour lire le fichier
#grep pour afficher seulement les commandes mise en arguments.

date -d @$first
# @ va convertir le time stamp en date humaine.

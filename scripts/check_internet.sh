#!/bin/bash

echo "Checking internet connection"

TEST=`ping -c 3 8.8.8.8 > /dev/null` # on va ping dns de google 3 fois

if [ $TEST -ne 0 ] #Si la connection ne fonctionne pas
	then
		echo "[/!\] Not connected to Internet[/!\]"
		echo "[/!\]Please Check Configuration[/!\]"

	else
		echo "internet access OK"
fi	

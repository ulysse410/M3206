#!/bin/bash

if (dpkg -s git | grep install > "Status: install ok installed");
then
	echo "[...]git : installé[...]"
else
	echo "[/!\]git n'est pas installé[/!\] lancer apt-get install git"
fi


if (dpkg -s tmux | grep install > "Status: install ok installed");
then
	echo "[...]tmux : installé[...]"
else
	echo "[/!\]tmux n'est pas installé[/!\] lancer apt-get install tmux"
fi

if (dpkg -s vim | grep install > "Status: install ok installed");
then
	echo "[...]vim : installé[...]"
else
	echo "[/!\]vim n'est pas installé[/!\] lancer apt-get install vim"
fi

if (dpkg -s htop | grep install > "Status: install ok installed");
then
	echo "[...]htop : installé[...]"
else
	echo "[/!\]htop n'est pas installé[/!\] lancer apt-get install htop"

fi

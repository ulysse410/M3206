#!/bin/bash
if (ssh -V > "OpenSSH *");
then
	echo "[...]SSH est installé[...]"
else
	echo"[/!\]SSH n'est pas installé[/!\]"
fi

if (service ssh status | grep active > "Active: active");
then
	echo "[...]SSH: le service est lancé[...]"
else
	echo "[/!\]SSH: le service n'est pas lancé[/!\]"
	echo "[...]lancement du service[...]"
	service ssh start >> /dev/null
fi



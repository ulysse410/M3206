#!/bin/bash

if [ "$0" == "./monjeu.sh" ]; then
	echo "Le script est nommé et lancé correctement"
else
	echo "Modifier le nom de ce script en monjeu.sh et exécuter le avec ./monjeu.sh"
	exit 1
fi

if [ -f "/m3206/scripts/test" ];then
	echo " Le fichier existe bel et bien "
else
	echo "le fichier test n'existe pas, veuillez le creer"
	exit 1
fi

if [ ls -l test | grep rw-r----- ];then
	echo "Le fichier a les droits necessaires
else
	echo "Modifier les droits du fichier test en rw-r-----"
	exit 1
fi

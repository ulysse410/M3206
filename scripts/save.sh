#!/bin/bash

path=$(readlink -f $1) #Pour avoir le chemin absolue en parametre

DATE=$(date +%Y_%m_%d_%H%M) #Pour avoir du type AAAA_mm_dd_HHMM

echo $path > $path/path.txt #on recopie dans un fichier

TARGET=/tmp/backup/$(date +%Y_%m_%d_%H%M)_$(basename $1).tar.gz #Chemin ou l'on va creer l'archive

tar cvzf $TARGET $1 #Creation
